﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace MakeHashedData
{
    public partial class MakeHashedDataForm : Form
    {
        public MakeHashedDataForm()
        {
            InitializeComponent();
        }

        // ハッシュ作成処理を入れる入れ物
        private delegate string MakeHash(string source);

        /// <summary>
        /// 閉じるボタン処理。
        /// </summary>
        /// <param name="sender">イベント呼び出し元オブジェクト</param>
        /// <param name="e">e</param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ハッシュデータ作成。
        /// </summary>
        /// <param name="sender">イベント呼び出し元オブジェクト</param>
        /// <param name="e">e</param>
        private void MakeHash_Click(object sender, EventArgs e)
        {
            if (SourceText.Text.Length > 0)
            {
                // とりあえずMD5用メソッドを初期値に設定。
                MakeHash hashFunction = MakeHash_MD5;
                switch (HashType.Text)
                {
                    case "MD5(16bytes)":
                        // MD5でハッシュデータを作る
                        hashFunction = MakeHash_MD5;
                        break;
                    case "SHA1(20bytes)":
                        // SHA1でハッシュデータを作る
                        hashFunction = MakeHash_SHA1;
                        break;
                    case "SHA256(32bytes)":
                        // SHA256でハッシュデータを作る
                        hashFunction = MakeHash_SHA256;
                        break;
                    case "SHA384(48bytes)":
                        // SHA384でハッシュデータを作る
                        hashFunction = MakeHash_SHA384;
                        break;
                    case "SHA512(64bytes)":
                        // SHA384でハッシュデータを作る
                        hashFunction = MakeHash_SHA512;
                        break;
                    case "PBKDF2":
                        // PBKDF2でハッシュデータを作る
                        hashFunction = MakeHash_PBKDF2;
                        break;
                    case "BCrypt":
                        // BCryptでハッシュデータを作る
                        hashFunction = MakeHash_BCrypt;
                        break;
                }
                HashedText.Text = hashFunction(SourceText.Text);
            }
        }

        /// <summary>
        /// MD5を使ってハッシュを作成する。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_MD5(string source)
        {
            return BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(source))).Replace("-", string.Empty);
        }

        /// <summary>
        /// SHA1を使ってハッシュを作成する。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_SHA1(string source)
        {
            return BitConverter.ToString(new SHA1CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(source))).Replace("-", string.Empty);
        }

        /// <summary>
        /// SHA256を使ってハッシュを作成する。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_SHA256(string source)
        {
            return BitConverter.ToString(new SHA256CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(source))).Replace("-", string.Empty);
        }

        /// <summary>
        /// SHA384を使ってハッシュを作成する。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_SHA384(string source)
        {
            return BitConverter.ToString(new SHA384CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(source))).Replace("-", string.Empty);
        }

        /// <summary>
        /// SHA512を使ってハッシュを作成する。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_SHA512(string source)
        {
            return BitConverter.ToString(new SHA512CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(source))).Replace("-", string.Empty);
        }

        /// <summary>
        /// PBKDF2(Password-Based Key Drivation Function 2)を使ってハッシュを作成する。
        /// ソフトは「saltsalt」、長さは32bytesで指定（変更可能）。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_PBKDF2(string source)
        {
            // Temporarily, salt is set  to word "saltsalt" (at least it needs 8 bytes word for salt).
            // If you need, specify a iteration number on 3rd parameter of Rfc2898DeriveBytes method.
            return BitConverter.ToString(new Rfc2898DeriveBytes(source, Encoding.UTF8.GetBytes("saltsalt")).GetBytes(32)).Replace("-", string.Empty);
        }

        /// <summary>
        /// BCryptを使ってハッシュを作成する。
        /// 別途ライブラリが必要。NuGetで入らない場合は https://archive.codeplex.com/?p=bcrypt を参照して対応してください。
        /// ソフトは「saltsalt」、長さは32bytesで指定（変更可能）。
        /// </summary>
        /// <param name="source">対象文字列。</param>
        /// <returns>ハッシュデータ。</returns>
        private string MakeHash_BCrypt(string source)
        {
            return BCrypt.Net.BCrypt.HashPassword(source);
        }

        /// <summary>
        /// フォーム初期化処理。
        /// </summary>
        /// <param name="sender">イベント呼び出し元オブジェクト</param>
        /// <param name="e">e</param>
        private void MakeHashedDataForm_Load(object sender, EventArgs e)
        {
            HashType.SelectedIndex = 0;
        }
    }
}
