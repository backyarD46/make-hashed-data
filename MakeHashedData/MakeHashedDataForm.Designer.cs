﻿namespace MakeHashedData
{
    partial class MakeHashedDataForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.SourceText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.HashType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.HashedText = new System.Windows.Forms.TextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.MakeHashButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SourceText
            // 
            this.SourceText.Location = new System.Drawing.Point(13, 33);
            this.SourceText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SourceText.Multiline = true;
            this.SourceText.Name = "SourceText";
            this.SourceText.Size = new System.Drawing.Size(529, 116);
            this.SourceText.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "ハッシュ化する文章";
            // 
            // HashType
            // 
            this.HashType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HashType.FormattingEnabled = true;
            this.HashType.Items.AddRange(new object[] {
            "MD5(16bytes)",
            "SHA1(20bytes)",
            "SHA256(32bytes)",
            "SHA384(48bytes)",
            "SHA512(64bytes)",
            "PBKDF2",
            "BCrypt"});
            this.HashType.Location = new System.Drawing.Point(150, 157);
            this.HashType.Name = "HashType";
            this.HashType.Size = new System.Drawing.Size(271, 27);
            this.HashType.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "ハッシュ方式";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "ハッシュ化データ";
            // 
            // HashedText
            // 
            this.HashedText.Location = new System.Drawing.Point(13, 217);
            this.HashedText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.HashedText.Multiline = true;
            this.HashedText.Name = "HashedText";
            this.HashedText.Size = new System.Drawing.Size(529, 116);
            this.HashedText.TabIndex = 4;
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(446, 350);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(95, 31);
            this.CloseButton.TabIndex = 6;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // MakeHashButton
            // 
            this.MakeHashButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.MakeHashButton.Location = new System.Drawing.Point(427, 157);
            this.MakeHashButton.Name = "MakeHashButton";
            this.MakeHashButton.Size = new System.Drawing.Size(114, 27);
            this.MakeHashButton.TabIndex = 7;
            this.MakeHashButton.Text = "作成";
            this.MakeHashButton.UseVisualStyleBackColor = true;
            this.MakeHashButton.Click += new System.EventHandler(this.MakeHash_Click);
            // 
            // MakeHashedDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(553, 391);
            this.Controls.Add(this.MakeHashButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.HashedText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HashType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SourceText);
            this.Font = new System.Drawing.Font("Meiryo UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MakeHashedDataForm";
            this.Text = "ハッシュデータ作成";
            this.Load += new System.EventHandler(this.MakeHashedDataForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SourceText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox HashType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox HashedText;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button MakeHashButton;
    }
}

